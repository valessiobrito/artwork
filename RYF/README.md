# RYF - Respects Your Freedom

## About

The "Respects Your Freedom" computer hardware product certification program encourages the creation and sale of hardware that will do as much as possible to respect your freedom and your privacy, and will ensure that you have control over your device.  More: https://www.fsf.org/ryf

## ToDo

- New generic Flyer - OK
- Proposal to a new website - OK
- Codding CSS/HTML5 Template - Pending
- Apply Template on Drupal - Pending


