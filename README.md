# ArtWork

Repository of visual works. Graphic, printed, website designs, logos, advertising and others.

All works reproduced in this repository is licensed under GPLv3 or compatible,
by Valessio Soares de Brito <contato@valessiobrito.com.br>,
unless explicitly specified in some other README in a subdirectory.

About license GPLv3, see: LICENSE
